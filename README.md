# README #
angular(v4) + express + mysql 을 사용하는 시드 프로젝트입니다.
우분투 환경에서 Nodejs와 Mysql이 이미 설치되어 있는 환경을 기준으로 설명합니다.

기준 디렉토리 : ~/project/angular-seed/
앱 이름: abroaden

## 1. express-cli 로 express프로젝트 생성
참고: http://expressjs.com/ko/starter/generator.html
```
$ sudo npm install express-generator -g
$ cd ~/project/angular-seed/
$ express abroaden
```
## 2. anuglar-cli 설치 및 클라이언트 프로젝트 생성
참고: https://github.com/angular/angular-cli
```
$ sudo npm install -g @angular/cli
$ cd ~/project/angular-seed/abroaden
$ ng new client
```

## 3. express 설정

### 3.1 ~/project/angular-seed/abroaden/app.js 수정
도메인 root 경로로 이동했을 때  ~/project/angular-seed/abroaden/client/dist/index.html 파일로 이동시킵니다.

아래와 같이 코드를 수정합니다.

```javascript
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'client/dist'));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client/dist')));

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  next();
});

app.use('/', index);

app.use(function (req, res, next) {
  return res.render('index');
});

module.exports = app;
```

## 4. angular 빌드
- angular-cli로 프로젝트를 생성하면 js가 아닌 ts확장명이 있는 것을 볼 수 있습니다.
- ts확장명은 typescript파일을 의미하며 typescript 파일은 js파일로 빌드 되어 html 파일에 삽입됩니다.
- 아래는 angular-cli로 만든 프로젝트를 빌드하는 과정입니다.
```
$ cd ~/project/angular-seed/abroaden/client
$ ng build
```

- 빌드하면 ~/project/angular-seed/abroaden/dist/ 디렉토리 안에 빌드된 js 파일들이 생성된 것을 확인할 수 있습니다. js파일들은 index.html파일에 로드되어 사용됩니다.
- express에서 root경로를 이 index.html파일로 해놨기 때문에 서버를 실행하고 접속하면 이 파일이 보여질 것입니다.

## 5. express 실행 테스트

### 5.1 nodemon 설치
  - 서버 실행 중에 소스가 변경되도 감지하지 않습니다.
  - nodemon이란 npm 패키지를 설치하고 package.json파일을 수정하면 소스가 변경될 때마다 서버가 재시작합니다.
  ```
  $ cd ~/project/angular-seed/abroaden
  $ sudo npm install --save nodemon
  ```
### 5.2  package.json 수정
  - ~/project/angular-seed/abroaden/package.json 파일의 아래 라인을 수정합니다.
  ```
  "start": "node ./bin/www" -> "start": "nodemon ./bin/www"
  ```
### 5.3 express 서버 실행
- express 프로젝트 폴더로 이동해서 npm install로 패키지들을 설치한 후 npm start 명령을 실행합니다.
- 기본 포트는 3000이고 ~/project/angular-seed/abroaden/bin/www 파일에서 변경할 수 있습니다.
- app works! 가 뜨면 express와 angular 세팅이 완료된 것입니다.
```
$ cd ~/project/angular-seed/abroaden
$ npm install
$ npm start
```

## 6. mysql 세팅

### 6.1 node용 mysql패키지 설치
```
$ cd ~/project/angular-seed/abroaden
$ npm install --save mysql
```

### 6.2 db.js 추가
- ~/project/angular-seed/abroaden/db.js를 추가하고 아래와 같이 입력합니다.
- dbconfig의 내용은 사용할 db정보로 수정해서 사용합니다.
- 사용법은 tomato/admin프로젝트의 routes파일들을 참고하시면 될 것 같습니다.

```javascript
var mysql = require('mysql');
var dbconfig = {
  connectionLimit: 100,
  host: 'jakethecoder.com',
  port: 3306,
  user: 'tomato',
  password: 'Tomato0330!',
  database: 'tomato'
}

var pool = mysql.createPool(dbconfig);

module.exports.query = function (sql, data, next) {
  if (arguments.length === 2) {
    next = data;
    data = null;
  }

  if (!pool) {
    console.log('connection pool is not found');
    next(new Error('connection pool is not found'));
    return;
  }

  pool.getConnection(function (err, connection) {
    if (err) {
      next(err);
      return;
    }

    connection.query(sql, data, function (err, results) {
      connection.release();
      next(err, results);
    });
  });
}
```